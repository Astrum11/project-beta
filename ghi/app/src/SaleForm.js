import React, { useState, useEffect } from 'react';

function SaleForm({ getSale }) {
  const [automobile, setAutomobile] = useState('');
  const [automobiles, setAutomobiles] = useState([]);
  const [salesPerson, setSalesPerson] = useState('');
  const [salespeople, setSalespeople] = useState([]);
  const [customer, setCustomer] = useState('');
  const [customers, setCustomers] = useState([]);
  const [price, setPrice] = useState('');

  useEffect(() => {
    async function fetchData() {
      const automobileResponse = 'http://localhost:8100/api/automobiles/';
      const salespeopleResponse = 'http://localhost:8090/api/salespeople/';
      const customersResponse = 'http://localhost:8090/api/customers/';
      const autoResponse = await fetch(automobileResponse);
      const salesPeopleResponses = await fetch(salespeopleResponse);
      const customerResponses = await fetch(customersResponse);

      if (autoResponse.ok) {
        const automobileData = await autoResponse.json();
        const unsoldAutomobiles = automobileData.autos.filter(auto => !auto.sold);
        setAutomobiles(unsoldAutomobiles);
      }

      if (salesPeopleResponses.ok) {
        const salespeopleData = await salesPeopleResponses.json();
        setSalespeople(salespeopleData.salespeople);
      }

      if (customerResponses.ok) {
        const customersData = await customerResponses.json();
        setCustomers(customersData.customers);
      }
    }

    fetchData();
  }, []);

  async function handleSubmit(event) {
    event.preventDefault();

    // Get the selected automobile object from the state
    const selectedAutomobile = automobiles.find(auto => auto.id === parseInt(automobile));

    // Get the selected customer object from the state
    const selectedCustomer = customers.find(cust => cust.id === parseInt(customer));

    // Get the selected salesperson object from the state
    const selectedSalesperson = salespeople.find(sales => sales.id === parseInt(salesPerson));

    // Check if any selected object is undefined
    if (!selectedAutomobile || !selectedCustomer || !selectedSalesperson) {
      // Handle the error case where one or more selected objects are missing
      console.error('Selected object is missing');
      return;
    }

    // Prepare the data object with the required details
    const data = {
      price,
      salesperson: {
        first_name: selectedSalesperson.first_name,
        last_name: selectedSalesperson.last_name,
        employee_id: selectedSalesperson.employee_id,
        name: `${selectedSalesperson.first_name} ${selectedSalesperson.last_name}`,
      },
      customer: {
        first_name: selectedCustomer.first_name,
        last_name: selectedCustomer.last_name,
        name: `${selectedCustomer.first_name} ${selectedCustomer.last_name}`,
      },
      automobile: {
        vin: selectedAutomobile.vin,
      },
    };

    const saleURL = 'http://localhost:8090/api/sales/';
    const fetchConfig = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(saleURL, fetchConfig);
    if (response.ok) {
      getSale();

    }
    event.target.reset();

  }

  function handleAutomobileChange(event) {
    setAutomobile(event.target.value);
  }

  function handleSalesPersonChange(event) {
    setSalesPerson(event.target.value);
  }

  function handleCustomerChange(event) {
    setCustomer(event.target.value);
  }

  function handlePriceChange(event) {
    setPrice(event.target.value);
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h2>Record a New Sale</h2>
          <form onSubmit={handleSubmit}>
            <div className="mb-3">
              <label htmlFor="automobile" className="form-label">Choose an Automobile VIN:</label>
              <select
                id="automobile"
                value={automobile}
                onChange={handleAutomobileChange}
                className="form-select"
                required
              >
                <option value="">Select an Automobile VIN</option>
                {automobiles.map((automobile) => (
                  <option key={automobile.id} value={automobile.id}>
                    {automobile.vin}
                  </option>
                ))}
              </select>
            </div>

            <div className="mb-3">
              <label htmlFor="salesperson" className="form-label">Choose a Salesperson:</label>
              <select
                id="salesperson"
                value={salesPerson}
                onChange={handleSalesPersonChange}
                className="form-select"
                required
              >
                <option value="">Select a Salesperson</option>
                {salespeople.map((salesperson) => (
                  <option key={salesperson.id} value={salesperson.id}>
                    {`${salesperson.first_name} ${salesperson.last_name}`}
                  </option>
                ))}
              </select>
            </div>

            <div className="mb-3">
              <label htmlFor="customer" className="form-label">Choose a Customer:</label>
              <select
                id="customer"
                value={customer}
                onChange={handleCustomerChange}
                className="form-select"
                required
              >
                <option value="">Select a Customer</option>
                {customers.map((customer) => (
                  <option key={customer.id} value={customer.id}>
                    {`${customer.first_name} ${customer.last_name}`}
                  </option>
                ))}
              </select>
            </div>

            <div className="mb-3">
              <label htmlFor="price" className="form-label">Price:</label>
              <input
                id="price"
                required
                type="text"
                value={price}
                onChange={handlePriceChange}
                className="form-control"
              />
            </div>

            <button type="submit" className="btn btn-primary">Record Sale</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default SaleForm;
