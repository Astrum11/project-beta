import { BrowserRouter, Routes, Route } from 'react-router-dom';
import React, { useState, useEffect } from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import AutomobileForm from './AutomobileForm';
import ListAutomobiles from './ListAutomobiles';
import ManufacturersList from './ManufacturersList';
import ManufacturersForm from './ManufacturersForm';
import VehicleList from './VehicleList';
import VehicleForm from './VehicleForm';
import SalespersonForm from './SalespersonForm';
import SalespersonList from './SalespersonList';
import CustomerList from './CustomerList.js';
import CustomerForm from './CustomerForm.js';
import SaleForm from './SaleForm';
import SaleList from './SaleList';
import TechnicianForm from './TechnicianForm';
import ListTechnicians from './ListTechnicians';
import SalesHistory from './SalesHistory';
import AppointmentForm from './AppointmentForm';
import ListAppointments from './ListAppointments';
import ServiceHistory from './ServiceHistory';

function App() {
  const [manufacturers, setManufacturers] = useState([]);
  const [vehicle, setVehicle] = useState([]);
  const [salespeople, setSalespeople] = useState([]);
  const [automobiles, setAutomobiles] = useState([]);
  const [technicians, setTechnicians] = useState([]);
  const [models, setModels] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [sales, setSales] = useState([]);
  const [appointments, setAppointments] = useState([]);

  async function getAppointments () {
    const url = "http://localhost:8080/api/appointments/"
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
    } else {
      console.error(response);
    }
  }

  async function getSales () {
    const url = "http://localhost:8090/api/sales/"
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setSales(data.sales);
    } else {
      console.error(response);
    }
  }

  async function loadAutomobiles() {
    const url = "http://localhost:8100/api/automobiles/"
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.autos);
    } else {
      console.error(response);
    }
  }

  async function getTechnicians() {
    const url = "http://localhost:8080/api/technicians/"
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    } else {
      console.error(response);
    }
  }


  async function getManufacturers() {
    const url = 'http://localhost:8100/api/manufacturers/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    } else {
      console.error(response);
    }
  }

  async function getModels() {
    const url = "http://localhost:8100/api/models/"
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    } else {
      console.error(response);
    }
  }
  async function getVehicle() {
    const url = 'http://localhost:8100/api/models/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setVehicle(data.models);
    }
  }
  async function getSalespeople() {
    const url = 'http://localhost:8090/api/salespeople/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salespeople);
    } else {
      console.error(response);
    }
  }

  async function getCustomer() {
    const url = 'http://localhost:8090/api/customers/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    } else {
      console.error(response);
    }
  }

  useEffect(() => {
    loadAutomobiles();
    getModels();
    getVehicle();
    getManufacturers();
    getSalespeople();
    getCustomer();
    getSales();
    getTechnicians();
    getAppointments();
  }, []);




  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="automobiles">
            <Route index element= {<ListAutomobiles automobiles={automobiles} models={models} loadAutomobiles={loadAutomobiles} />} />
            <Route path='new' element= {<AutomobileForm getModels={getModels} loadAutomobiles={loadAutomobiles}/>} />
          </Route>
          <Route path="manufacturers">
            <Route path="new" element={<ManufacturersForm getManufacturers={getManufacturers} />} />
            <Route index element={<ManufacturersList manufacturers={manufacturers} getManufacturers={getManufacturers} />} />
          </Route>
          <Route path="vehicle">
            <Route path="new" element={<VehicleForm getVehicle={getVehicle} />} />
            <Route index element={<VehicleList vehicle={vehicle} getVehicle={getVehicle} />} />
          </Route>
          <Route path="salespeople">
            <Route path="new" element={<SalespersonForm getSalespeople={getSalespeople} />} />
            <Route index element={<SalespersonList salespeople={salespeople} getSalespeople={getSalespeople} />} />
          </Route>
          <Route path="customers">
            <Route path="new" element={<CustomerForm getCustomer={getCustomer} />} />
            <Route index element={<CustomerList customers={customers} />} />
          </Route>
          <Route path="sales">
            <Route path="new" element={<SaleForm getSales={getSales} />} />
            <Route path="history" element={<SalesHistory sales={sales} />} />
            <Route index element={<SaleList sales={sales} />} />
          </Route>
          <Route path="technicians">
            <Route path="new" element={<TechnicianForm getTechnicians={getTechnicians} />} />
            <Route index element={<ListTechnicians technicians={technicians} getTechnicians={getTechnicians} />} />
          </Route>
          <Route path="appointments">
            <Route path="new" element={<AppointmentForm technicians={technicians} getTechnicians={getTechnicians} />} />
            <Route path="history" element={<ServiceHistory/>} />
            <Route index element={<ListAppointments appointments={appointments} />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}


export default App;
