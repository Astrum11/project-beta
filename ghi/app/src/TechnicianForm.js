import React, { useState } from 'react';



function TechnicianForm({ getTechnicians }) {


  const [name, setName] = useState('');
  const [employee_id, setEmployeeId] = useState('');


  async function handleSubmit(event) {
    event.preventDefault();
    const data = {
      name,
      employee_id
    };

    const url = 'http://localhost:8080/api/technicians/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      getTechnicians();
    } else {
      console.error(response);

    }
    event.target.reset();
  }


  function handleNameChange(event) {
    const value = event.target.value;
    setName(value);
  }

  function handleEmployeeIdChange(event) {
    const value = event.target.value;
    setEmployeeId(value);
  }

  return (
    <div>
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new Technician</h1>
          <form onSubmit={handleSubmit} id="create-technician-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleNameChange}
                placeholder="Name"
                required
                type="text"
                id="name"
                name="name"
                className="form-control"
              />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleEmployeeIdChange}
                placeholder="Employee ID"
                required
                type="text"
                id="employee_id"
                name="employee_id"
                className="form-control"
              />
              <label htmlFor="employee_id">Employee ID</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
    </div>
  );
}

export default TechnicianForm;
