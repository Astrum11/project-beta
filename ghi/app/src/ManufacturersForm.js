import React, { useState, useEffect } from 'react';

function ManufacturersForm({ getManufacturers }) {
  const [name, setName] = useState('');

  async function handleSubmit(e) {
    e.preventDefault();
    const data = {
      name: name,
    };

    const url = 'http://localhost:8100/api/manufacturers/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      // Call the getManufacturers function to update the list of manufacturers
      getManufacturers();
    } else {
      console.error(response);
    }

    e.target.reset();
    // navigate('/manufacturers');
  }

  function handleNameChange(e) {
    const value = e.target.value;
    setName(value);
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new Manufacturer</h1>
          <form onSubmit={handleSubmit} id="create-manufacturer-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleNameChange}
                placeholder="name"
                required
                type="text"
                id="name"
                name="name"
                className="form-control"
              />
              <label htmlFor="name">Manufacturer Name</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ManufacturersForm;
