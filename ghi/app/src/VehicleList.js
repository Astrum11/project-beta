function VehicleList({ vehicle }) {
  return(
      <table className="table table-dark table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Manufacturer</th>
          <th>Picture</th>
        </tr>
      </thead>
      <tbody>
        {vehicle.map(vehicle => {
          return (
          <tr key={vehicle.id}>
            <td>{vehicle.name}</td>
            <td>{vehicle.manufacturer.name}</td>
            <td><img src={vehicle.picture_url} alt={vehicle.name} /></td>
          </tr>
          );
        })}
      </tbody>
    </table>
  )
}

export default VehicleList;
