import React, { useState, useEffect } from 'react';

function SaleList() {
    const [ listSale, setListSale ] = useState([]);
    useEffect(() => {
        async function getSaleList() {
            const url = "http://localhost:8090/api/sales/"
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setListSale(data.sales);
            }
        }
        getSaleList();
    }, []);

  return (
    <table className="table table-dark table-striped">
      <thead>
        <tr>
          <th>Salesperson Employee ID</th>
          <th>Salesperson</th>
          <th>Customer</th>
          <th>VIN</th>
          <th>Price</th>
        </tr>
      </thead>
      <tbody>
        {listSale.map((sale) => {
            return (
              <tr key={sale.id}>
                <td>{sale.salesperson.employee_id}</td>
                <td>{`${sale.salesperson.first_name} ${sale.salesperson.last_name}`}</td>
                <td>{`${sale.customer.first_name} ${sale.customer.last_name}`}</td>
                <td>{sale.automobile.vin}</td>
                <td>{sale.price}</td>
              </tr>
            );
        })}
      </tbody>
    </table>
  );
}
export default SaleList;
