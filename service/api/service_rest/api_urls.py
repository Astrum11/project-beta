from django.urls import path
from .api_views import api_detail_technician, api_list_appointments, api_list_technicians, api_detail_appointment

urlpatterns = [
    path('technicians/', api_list_technicians, name="api_list_technicians"),
    path('technicians/<int:id>/', api_detail_technician, name="api_detail_technician"),
    path('appointments/', api_list_appointments, name="api_list_appointments"),
    path('appointments/history', api_list_appointments, name="api_list_appointments"),
    path('appointments/history/<int:vin>/', api_list_appointments, name="api_list_appointments"),
    path('appointments/<int:id>/', api_detail_appointment, name="api_detail_appointment"),
    path('appointments/<int:id>/cancel', api_detail_appointment, name="api_detail_appointment"),
    path('appointments/<int:id>/finish', api_detail_appointment, name="api_detail_appointment"),
]
