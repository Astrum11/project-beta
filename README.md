# CarCar

CarCar is an application which revolves around three central components of a car dealership; namely inventory, sales and service.

Team:

* **Michael Nguyen** - Sales Microservice
* **Edward Lee** - Service Microservice


## How to Run this App

	Repository Link: https://gitlab.com/Astrum11/project-beta

	1. Fork project repository
	2. Clone the project repository to your local machine.
	3. Navigate to the project directory.
	4. Install the necessary dependencies for the front-end and back-end by running the following commands:

	- npm install
	- pip install -r requirements.txt

	then:

    docker volume create beta-data
    docker-compose build
    docker-compose up

## Design

![Alt text](design.png)

## API Documentation: Using Insomnia

######

## URLS and Ports

        port 3000, may take 2-5 mins to load after 'docker-compose up'

        Main Page: http://localhost:3000/

    **Sales on Main Page**
        List Salespeople: http://localhost:3000/salespeople
        New Salesperson: http://localhost:3000/salespeople/new
        List Customers: http://localhost:3000/customers
        New Customer: http://localhost:3000/customers/new
        List Sales: http://localhost:3000/sales/
        New Sale: http://localhost:3000/sales/new
        Sale history: http://localhost:3000/sales/history

	**Services on Main Page**
        List Appointments: http://localhost:3000/appointments
        Create Appointment: http://localhost:3000/appointments/new
        List Technicians: http://localhost:3000/technicians/
        New Technician: http://localhost:3000/technicians/new
        List Service history: http://localhost:3000/appointments/history



# Sales:
    Port 8090 for Insomnia
    port 3000 for React Front End
    /api/salespeople/
    /api/customers/
    /api/sales/


# Service:
    Port 8080 for Insomnia
    port 3000 for React Front End
    /api/appointments/
    /api/technicians/

# Salesperson:
    Port 8080 for Insomnia
    port 3000 for React Front End

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List Salesperson | GET | http://localhost:8090/api/salespeople/
| Salesperson details | GET | http://localhost:8090/api/salespeople/<int:id>
| Create Salesperson | POST | http://localhost:8090/api/salespeople/
| Delete Salesperson | DELETE | http://localhost:8090/api/salespeople/<int:id>
| Update Salesperson | PUT | http://localhost:8090/api/salespeople/<int:id>

#### List salespeople (GET):
http://localhost:8090/api/salespeople/
Returns a list of all salespeople
```json
{
	"salespeople": [
		{
			"id": 1,
			"first_name": "John",
			"last_name": "Soul",
			"employee_id": "12345das"
		}
	]
}
```
#### Get a specific salesperson (GET):
http://localhost:8090/api/salesperson/:id/

The list of salespeople is a dictionary with the key 'salespeople' set to a list of salespeople.
```json
{
	"id": 1,
	"first_name": "John",
	"last_name": "Soul",
	"employee_id": "12345das"
}
```
#### Create a new salesperson (POST):
http://localhost:8090/api/salespeople/

Creating a new Salesperson requires the first_name, last_name, and the employee_id
```json
{
	"first_name": "John",
	"last_name": "Soul",
	"employee_id": "12345das"
}
```
#### Update a specific salesperson (PUT):
http://localhost:8090/api/salespeople/:id/

Updating a Salesperson requires the first_name, last_name, and the employee_id
```json
{
	"first_name": "John",
	"last_name": "Soulss",
	"employee_id": "12345das"
}
```
#### Delete a specific salesperson(DELETE):
http://localhost:8090/api/salespeople/:id/
Delete an salesperson. Replace int:id  in the request url with an salesperson's "id".
## Customers
    port 8090 for Insomnia
    port 3000 for React Front End

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List Customers | GET | http://localhost:8090/api/customers/
| Customer details | GET | http://localhost:8090/api/sales/<int:id>
| Create Customer | POST | http://localhost:8090/api/sales/
| Delete Custoemr | DELETE | http://localhost:8090/api/customer/<int:id>

### List customers (GET):
http://localhost:8090/api/customers/
Returns a list of all customers
``` json
{
	"customers": [
		{
			"id": 1,
			"first_name": "Jane",
			"last_name": "Greene",
			"address": "678 Arroyo Ave. , GA, 28213",
			"phone_number": "111-111-1111"
		}
	]
}
```
### Get a specific customer (GET):
http://localhost:8090/api/customers/:id/

The list of salespeople is a dictionary with the key 'customer' set to a list of customers.


``` json
{
	"id": 1,
	"first_name": "Jane",
	"last_name": "Greene",
	"address": "678 Arroyo Ave. , GA, 28213",
	"phone_number": "111-111-1111"
}
```

### Create a new customer (POST):
http://localhost:8090/api/customers/

```json
{
    "first_name": "Jane",
    "last_name": "Greene",
    "address": "678 Arroyo Ave. , GA, 28213",
	"phone_number": "111-111-1111",
}
```

### Update a specific customer (PUT):
http://localhost:8090/api/customers/:id/
Updating a Customer requires the first_name, last_name, address, and phone_number
```json
{
    "first_name": "Jane",
    "last_name": "Red",
    "address": "392 Cloud Lane. , CA , 92712",
	"phone_number": "222-222-2222"
}
```

### Delete a specific customer (DELETE):
http://localhost:8090/api/customers/:id/
Delete an customer. Replace int:id in the request url with an customer's "id".
# Sales:
    Port 8090 for Insomnia
    port 3000 for React Front End


| Action | Method | URL
| ----------- | ----------- | ----------- |
| List Sales | GET | http://localhost:8090/api/sales/
| Sale details | GET | http://localhost:8090/api/sales/<int:id>
| Sales history | GET | http://localhost:8090/api/sales/history
| Create Sale | POST | http://localhost:8090/api/sales/
| Delete Sale | DELETE | http://localhost:8090/api/sales/<int:id>

### List sale (GET):
http://localhost:8090/api/sales/
Returns a list of all Sales
```json
{
	"sales": [
		{
			"id": 1,
			"price": "$17.25",
			"salesperson": {
				"first_name": "John",
				"last_name": "Soul",
				"employee_id": "12345das"
			},
			"customer": {
				"first_name": "Jane",
				"last_name": "Greene"
			},
			"automobile": {
				"vin": "1C3CC5FB2AN120174"
			}
		},
		{
			"id": 2,
			"price": "$23.75",
			"salesperson": {
				"first_name": "John",
				"last_name": "Soul",
				"employee_id": "12345das"
			},
			"customer": {
				"first_name": "Jane",
				"last_name": "Greene"
			},
			"automobile": {
				"vin": "1C3CC5FB2AN120174"
			}
		},
		{
			"id": 3,
			"price": "$18.25",
			"salesperson": {
				"first_name": "John",
				"last_name": "Soul",
				"employee_id": "12345das"
			},
			"customer": {
				"first_name": "Jane",
				"last_name": "Greene"
			},
			"automobile": {
				"vin": "1C3CC5FB2AN120174"
			}
		},
		{
			"id": 4,
			"price": "$18.25",
			"salesperson": {
				"first_name": "John",
				"last_name": "Soul",
				"employee_id": "12345das"
			},
			"customer": {
				"first_name": "Jane",
				"last_name": "Greene"
			},
			"automobile": {
				"vin": "1C3CC5FB2AN120174"
			}
		}
	]
}
```
### Get a specific sale (GET):
http://localhost:8090/api/sales/:id/
The list of sales is a dictionary with the key 'sales' set to a list of sales.

```json
{
	"id": 1,
	"price": "$17.25",
	"salesperson": {
		"first_name": "John",
		"last_name": "Soul",
		"employee_id": "12345das"
	},
	"customer": {
		"first_name": "Jane",
		"last_name": "Greene"
	},
	"automobile": {
		"vin": "1C3CC5FB2AN120174"
	}
}
```

### Create a new sale (POST):
http://localhost:8090/api/sales/
```json
{
	"price": "$18.25",
	"salesperson": {
		"first_name": "John",
		"last_name": "Soul",
		"employee_id": "12345das"
	},
	"customer": {
		"first_name": "Jane",
		"last_name": "Greene"
	},
	"automobile": {
		"vin": "1C3CC5FB2AN120174"
	}
}

```
### Delete a specific sale(DELETE):
http://localhost:8090/api/sales/:id/




## SERVICES


### Technicians

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List technicians | GET | http://localhost:8080/api/technicians/
| Technician detail | GET | http://localhost:8080/api/technicians/<int:pk>/
| Create a technician | POST | http://localhost:8080/api/technicians/
| Delete a technician | DELETE | http://localhost:8080/api/technicians/<int:pk>/

LIST TECHNICIANS: Returns a list of all technicians.

GET: http://localhost:8080/api/technicians/
```json
{
	"technicians": [
		{
			"name": "Gornelius Malfitch",
			"employee_id": "gorn20",
			"id": 1
		},
		{
			"name": "Marklar Babowitz IV",
			"employee_id": "bab4",
			"id": 2
		},
		{
			"name": "Farquander Jakata",
			"employee_id": "4quad",
			"id": 3
		}
	]
}
```

DETAIL TECHNICIAN: List details for a specific technician. Replace <int:pk> in the request url with a technician's "id".

GET: http://localhost:8080/api/technicians/<int:pk>/

```json
{
	"name": "Joctavia Bandaxon",
	"employee_id": d4xon,
	"id": 4
}
```
ADD TECHNICIAN - Add a technician by making a POST request with the JSON body containing the technician's "name" and "employee_id". An "id" will be automatically generated.

POST: http://localhost:8080/api/technicians/
```json
{
	"name": "Winstonian McGee",
	"employee_id": w1nston,
}
```

DELETE TECHNICIAN - Delete a technician. Replace <int:pk> in the request url with a technician's "id".

DELETE: http://localhost:8080/api/technicians/<int:pk>/

### Appointments

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List Appointments | GET | http://localhost:8080/api/appointments/
| Appointment detail | GET | http://localhost:8080/api/appointments/<int:id>
| Appointment history | GET | http://localhost:8080/api/appointments/history
| Create Appointment | POST | http://localhost:8080/api/appointments/
| Delete Appointment | DELETE | http://localhost:8080/api/appointments/<int:id>


LIST APPOINTMENT: This will return a list of all currently scheduled appointments.

GET: http://localhost:8080/api/appointments/

```json
{
	"appointments": [
		{
			"id": 6,
			"vin": "324346123123",
			"datetime": "2023-06-23T17:24:00+00:00",
			"customer": "Yandina Shasz",
			"reason": "Wax and shine.",
			"vip": true,
			"status": true,
			"technician": {
				"name": "Marklar Babowitz IV",
				"employee_id": "bab4",
				"id": 2
			}
		},
		{
			"id": 10,
			"vin": "324346123123",
			"datetime": "2023-06-24T18:09:00+00:00",
			"customer": "Hingle McCringleberry",
			"reason": "Bigger rims.",
			"vip": true,
			"status": false,
			"technician": {
				"name": "Marklar Babowitz IV",
				"employee_id": "bab4",
				"id": 2
			}
		},
	]
}

```
APPOINTMENT DETAIL: This will return the detail of each specific service appointment. Replace <int:id> with the desired appointment's "id" value.

GET:  http://localhost:8080/api/appointments/<int:id>

```json

	{
		"id": 8,
		"vin": "111",
		"datetime": "2023-06-24T15:00:00+00:00",
		"customer": "Socradius Park",
		"reason": "Adding turbocharger.",
		"vip": false,
		"status": false,
		"technician": {
			"name": "Farquander Jagata",
			"employee_id": "4quad",
			"id": 3
		}
	}

```
SERVICE HISTORY: List all past service appointments which have been either "Completed" or "Deleted".

#### GET: http://localhost:8080/api/appointments/history

```json
{
	"appointments": [
		{
			"id": 5,
			"vin": "1231232",
			"datetime": "2023-06-30T16:20:00+00:00",
			"customer": "Bonegiorno Palarmya",
			"reason": "Oil changes.",
			"vip": false,
			"status": false,
			"technician": {
				"name": "Gornelius Malfitch",
				"employee_id": "gorn20",
				"id": 1
			}
		},
		{
			"id": 7,
			"vin": "123123123",
			"datetime": "2023-06-18T19:28:00+00:00",
			"customer": "Gringol Jettison",
			"reason": "Crocodile interior.",
			"vip": true,
			"status": false,
			"technician": {
				"name": "Joctavia Bandaxon",
				"employee_id": "d4xon",
				"id": 4
			}
		},
	]
}
```
CREATE APPOINTMENT - To create an appointment via insomnia, input the following JSON body in the proper format. In this case, the "datetime" value represents "June 24th, 2023" at "06:09PM." Technicians are referred to by their employee_id.

POST: http://localhost:8080/api/appointments/

```json
	{
		"vin":"324346123123",
		"datetime":"2023-06-24T18:09",
		"customer":"Hingle McCringleberry",
		"technician":"bab4",
		"reason":"Bigger rims."
	}


```
DELETE APPOINTMENT - Delete an appointment. Replace <int:pk> in the request url with an appointment's "id".

DELETE: http://localhost:8080/api/appointments/<int:pk>/



## Service microservice


The service microservice is comprised of 2 models with one representing technicians and the other appointments. These models interact with our inventory backend to provide maintenance, repairs, and follow up appointments with current and prospective customers.

This microservice keeps track of automobiles by VIN which it recieves via poller interaction with the inventory microservice and can designate VIP status for owners of all vehicles purchased at the dealership.

Aside from documenting all pertinent details for our working technicians, the main function of this service is to schedule appointments. Appointments can be created and assigned to a specific technician as well as be marked for completion or deletion. All past and current service records will be stored for future reference under the "Service History" page.


## Sales microservice

The sales microservice has 4 models: AutomobileVO, Customer, Salesperson, and Sales. Sales is the model that interacts with the other three models. This model gets data from the three other models.
​
The AutomobileVO is a value object that gets data about the automobiles in the inventory using a poller. The sales poller automotically polls the inventory microservice for data, so the sales microservice is constantly getting the updated data.
​
The reason for integration between these two microservices is that when recording a new sale, you'll need to choose which car is being sold and that information lives inside of the inventory microservice.
